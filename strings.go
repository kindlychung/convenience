package convenience

import (
	"math/rand"
	"strings"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyz" +
	"ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

// RandStringWithCharset generate a random string by sampling from a charset
func RandStringWithCharset(length int, charset string) string {
	b := make([]byte, length)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	return string(b)
}

// RandString generate a fixed width random string, using the charset 0-9a-zA-Z
func RandString(length int) string {
	return RandStringWithCharset(length, charset)
}

// Cieq case-insensitive comparison for two strings
func Cieq(s1, s2 string) bool {
	return strings.ToLower(s1) == strings.ToLower(s2)
}
