package convenience

import (
	"fmt"
	"log"
	"testing"

	"github.com/gen2brain/beeep"
)

// CheckErr log fatal if there is an error
func CheckErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

// BubbleErr send a notification message if there is error
func BubbleErr(err error, title string) {
	if err != nil {
		msg := fmt.Sprintf("%s", err)
		err := beeep.Alert(title, msg, "assets/warning.png")
		if err != nil {
			log.Fatal(err)
		}
	}
}

// Bark report error in the test functions
func Bark(t *testing.T, err error) {
	if err != nil {
		t.Error(err)
	}
}
