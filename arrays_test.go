package convenience

import (
	"testing"
)

func TestBao(t *testing.T) {
	stringBao := NewStringBao([]string{"what", "a", "cold", "day", "it", "is", "to", "day"})
	if stringBao.FindFirst("day") != 3 {
		t.Error("")
	}
	if stringBao.Find("day", 4) != 7 {
		t.Error("")
	}
}
