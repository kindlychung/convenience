package convenience

import (
	"bytes"
	"encoding/json"
)

// SSMap alias for map[string]string
type SSMap map[string]string

// UnmarshalJSON parse bytes into a string to string map
func (ssm *SSMap) UnmarshalJSON(b []byte) error {
	var temp []map[string]string
	if err := json.Unmarshal(b, &temp); err != nil {
		return err
	}
	for _, m := range temp {
		for k, v := range m {
			(*ssm)[k] = v
		}
	}
	return nil
}

// JSONDufDecoder creates a json decoder that disallows unknown fields, requires go 1.10 or later
func JSONDufDecoder(data []byte) *json.Decoder {
	decoder := json.NewDecoder(bytes.NewReader(data))
	decoder.DisallowUnknownFields()
	return decoder
}
